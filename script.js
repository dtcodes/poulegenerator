(function(){
	var players = ['Miep','Matthew','Jan','Koen','Fiona','Karel','Kees','Sjon','Miep','Matthew','Jan','Koen','Fiona','Karel','Kees','Sjon'];// players in this game

	var w = 1500, h = 1000;
	var numOfPools = 4; // number of pools to generate
	var rectWidth = 100;
	var rectHeight = 20;
	var startPosX = 20;
	var startPosY = 10;
	var lineX1 = 100;
	var lineX2 = 200;
	var linePos = 20;

	var svgContainer = d3.select("body").append("svg")
							 .attr("viewBox", "0 0 " + w + " " + h )
							 .attr("preserveAspectRatio", "xMinYMin");

	function shuffle(o){
	 for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	 return o;
	}

	function isInArray(value, array) {
	 return array.indexOf(value) > -1;
	}

	var isOdd = function(x) { return x & 1; };
	var isEven  = function(x) { return !( x & 1 ); };

	function calcBreakPoints(numOfPools, players, totalRect){
	 var arr = [z = numOfPools * 2];
	 var x = totalRect;
	 for(var i=0; i < x; i++){
	   arr.push(arr[arr.length-1] + z);
	 }
	 return arr;
	}

	function playersPerPool(players, numOfPools){
		return players.length / numOfPools;
	}

	/*
	 *	chunksFromPlayers
	 *	returns even chunks of the given players
	 *  input: array of players , number of pools
	 */
	function chunksFromPlayers(players, numOfPools){ // chunkies are working
		var perPool = playersPerPool(players, numOfPools);
		var chunks = [],i,j,temparray,chunk = perPool;

		for (i=0,j=players.length; i<j; i+=chunk) {
			temparray = players.slice(i,i+chunk);
			chunks.push(temparray);
		}
		return chunks;
	}

	/*
	 *	chunksToMatches
	 *	returns all possible matches in pools(arrays)
	 *  input: [['p1','p2'], ['p3','p4']]
	 */
	function chunksToMatches(chunksFromPlayers){
			var arr = [];
			for(var i = 0; i < chunksFromPlayers.length; i++){
				arr.push(matches(chunksFromPlayers[i]));
			}
			return arr;
	}

	console.log('Matches from the chunks: ');
	console.log(chunksToMatches(chunksFromPlayers(players, numOfPools), numOfPools));

	function matches(players){
	  var matches = [];
	  for(var i=0, j = 1; i < players.length; i++, j++){  // generates matches for the pool
	    var left = players.slice(i,j);
	    var right = players.slice(j);

	    for(var x=0; x < right.length; x++){
	      matches.push(Array(left[0], right[x]));
	    }
	  }
	  return matches;
	}
	/* error because if there are 3 pools the following row
	arr.push(chunks[i][r],chunks[x][r])
	is not working. then it has to be
	arr.push(chunks[i][r],chunks[x][r], chunks[y][r])

	implement breakpoints again??
	*/

	// function formatPlayerData(numOfPools, chunks){
	//
	// 	console.log(chunks);
	//
	//
	// 	var arr = [];
	//
	// 	for(var i = 0, x = 1, y = 2; i < chunks.length / numOfPools; i++, x++, y++){
	// 		for(var r = 0; r < chunks[i].length; r++){
	// 			arr.push(chunks[i][r],chunks[x][r]);
	// 		}
	// 	}
	// 	return [].concat.apply([], arr);
	// }

	function formatPlayerData(numOfPools, chunks){
		var arr = [];
		for(var i = 0; i < chunks.length / numOfPools; i++){
			for(var r = 0; r < chunks[i].length; r++){
				for(var x = 0; x < numOfPools; x++){
					arr.push(chunks[x][r]);
				}
			}
		}
		return [].concat.apply([], arr);
	}

	console.log('Formatted Data: ');
	console.log(formatPlayerData(numOfPools, chunksToMatches(chunksFromPlayers(players, numOfPools), numOfPools)))

	var breakPoints = calcBreakPoints(
		numOfPools,
		players,
		matches(players).length * 2);


	function drawDiagram(players){
		var r = matches(players).length;
		var diagramData = formatPlayerData(numOfPools, chunksToMatches(chunksFromPlayers(players, numOfPools), numOfPools));
		// console.log('Rectangles in the diagram: ' + r);
	  for (var i = 0,x = 0; i < r; i++, x++){
	    if(isInArray(i, breakPoints)){
	      startPosY = startPosY + 40;
	      startPosX = 20;
	      linePos = linePos + 40;
	      lineX1 = 100;
	      lineX2 = 200;
	      x = 0;
	    }
	    // svgContainer.append("rect")
	    //             .attr("x", startPosX * x * 10)
	    //             .attr("y", startPosY)
	    //             .attr("width", rectWidth)
	    //             .attr("height",rectHeight)
	    //             .attr("stroke", "black")
	    //             .style("stroke-width", 1)
	    //             .attr("fill", "none");
	    if(isEven(i)){
	    svgContainer.append("line")
	                .attr("x1", lineX1)
	                .attr("y1", linePos)
	                .attr("x2", lineX2)
	                .attr("y2", linePos)
	                .attr("stroke-width", 1)
	                .attr("stroke", "black");
	    }
	    lineX1 = lineX1 + 200;
	    lineX2 = lineX2+ 200;
			svgContainer.append("text")
			            .attr("x", startPosX * x * 10)
			            .attr("y", startPosY + 15)
			            .attr("fill", "black")
			            .text(diagramData[i])

	  }
	};

	drawDiagram(players);
})();
