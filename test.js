var p = [1,2,3,4,5,6];

function createMatches(players){
  var matches = [];
  for(var i=0, j = 1; i < players.length; i++, j++){  // generates matches for the pool
    var left = players.slice(i,j);
    var right = players.slice(j);

    for(var x=0; x < right.length; x++){
      matches.push(Array(left[0], right[x]));
    }
  }
  return matches;
}
